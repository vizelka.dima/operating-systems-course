#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <string>
#include <vector>
#include <array>
#include <iterator>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"


using namespace std;
#endif /* __PROGTEST__ */ 

class CCargoPlanner
{
public:
    static int               SeqSolver                     ( const vector<CCargo> & cargo,
                                                             int               maxWeight,
                                                             int               maxVolume,
                                                             vector<CCargo>  & load );
    void                     Start                         ( int               sales,
                                                             int               workers );
    void                     Stop                          ( void );

    void                     Customer                      ( ACustomer         customer );
    void                     Ship                          ( AShip             ship );

private:
    list<ACustomer> m_Customers;
    void Sell();
    void Work();
    vector<thread*> m_Workers;
    vector<thread*> m_Sellers;

    //async vars
    list<AShip> m_WorkerShips;
    list<AShip> m_SellerShips;
    map<string ,vector<CCargo>> m_Orders;
    bool lastShipWork = false;
    bool lastShipSell = false;

    condition_variable m_WorkCV;
    condition_variable m_SellCV;
    mutex m_WorkMX;
    mutex m_SellMX;
};

int CCargoPlanner::SeqSolver(const vector<CCargo> &cargo, int maxWeight, int maxVolume, vector<CCargo> &load)
{ return ProgtestSolver(cargo, maxWeight, maxVolume, load); }

void CCargoPlanner::Start(int sales, int workers) {
    for (int i = 0; i < sales; ++i)
    { m_Sellers.push_back(new thread(&CCargoPlanner::Sell, this)); }

    for (int i = 0; i < workers; ++i)
    { m_Workers.push_back(new thread(&CCargoPlanner::Work, this)); }
}

void CCargoPlanner::Stop(void) {
    lastShipSell = true;

    //notify_all_sellers
    m_SellCV.notify_all();
    for (auto s : m_Sellers)
    {
        s->join();
        delete s;
    }

    lastShipWork = true;

    //notify_all_workers
    m_WorkCV.notify_all();
    for (auto w : m_Workers)
    {
        w->join();
        delete w;
    }
}

void CCargoPlanner::Customer(ACustomer customer)
{ m_Customers.push_back(customer); }

void CCargoPlanner::Ship(AShip ship)
{
    m_SellerShips.push_back(ship);
    m_SellCV.notify_one();
}

void CCargoPlanner::Sell() {
    unique_lock<mutex> mxl(m_SellMX);

    while (true)
    {
        m_SellCV.wait(mxl,[&](){return (!m_SellerShips.empty() && !lastShipSell) || lastShipSell;});

        if(lastShipSell && m_SellerShips.empty())
            break;


        AShip ship = m_SellerShips.front();
        m_SellerShips.pop_front();
        mxl.unlock();

        for(auto const & customer : m_Customers)
        {
            vector<CCargo> cargos;
            customer->Quote(ship->Destination(), cargos);

            mxl.lock();
            if (m_Orders.find(ship->Destination()) == m_Orders.end())
                m_Orders.insert(make_pair(ship->Destination(),cargos));
            else
                m_Orders[ship->Destination()].insert( m_Orders[ship->Destination()].end(), cargos.begin(), cargos.end() );
            mxl.unlock();
        }

        mxl.lock();
        m_WorkerShips.push_back(ship);


        m_WorkCV.notify_one();
    }

}

void CCargoPlanner::Work() {
    //wait for notification (one)
    unique_lock<mutex> mxl(m_WorkMX);


    while(true)
    {

        m_WorkCV.wait(mxl,[&](){return (!m_WorkerShips.empty() && !lastShipWork) || lastShipWork;});


        if(lastShipWork && m_WorkerShips.empty())
        { break; }


        //-----------MUTEX_ON----------------
        AShip ship = m_WorkerShips.front();
        m_WorkerShips.pop_front();
        //-----------MUTEX_OFF---------------

        mxl.unlock();

        vector<CCargo> resCargos;
        SeqSolver(m_Orders[ship->Destination()], ship->MaxWeight(), ship->MaxVolume(), resCargos);

        ship->Load(resCargos);

        mxl.lock();
        m_Orders.erase(ship->Destination());
    }
}


//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int                main                                    ( void )
{
  CCargoPlanner  test;
  vector<AShipTest> ships;
    vector<ACustomerTest> customers { make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> (), make_shared<CCustomerTest> () };
    //vector<ACustomerTest> customers { make_shared<CCustomerTest> () };

  ships . push_back ( g_TestExtra[0] . PrepareTest ( "New York", customers ) );
  ships . push_back ( g_TestExtra[1] . PrepareTest ( "Barcelona", customers ) );
  ships . push_back ( g_TestExtra[2] . PrepareTest ( "Kobe", customers ) );
    ships . push_back ( g_TestExtra[8] . PrepareTest ( "Perth", customers ) );
    ships . push_back ( g_TestExtra[7] . PrepareTest ( "Perth2", customers ) );
    ships . push_back ( g_TestExtra[6] . PrepareTest ( "Perth3", customers ) );
    ships . push_back ( g_TestExtra[5] . PrepareTest ( "Perth4", customers ) );
    ships . push_back ( g_TestExtra[4] . PrepareTest ( "Perth5", customers ) );
    ships . push_back ( g_TestExtra[3] . PrepareTest ( "Perth6", customers ) );
    ships . push_back ( g_TestExtra[2] . PrepareTest ( "Perth7", customers ) );
    ships . push_back ( g_TestExtra[1] . PrepareTest ( "Perth8", customers ) );
    ships . push_back ( g_TestExtra[9] . PrepareTest ( "Perth9", customers ) );
    ships . push_back ( g_TestExtra[10] . PrepareTest ( "Pert", customers ) );
    ships . push_back ( g_TestExtra[11] . PrepareTest ( "Peh", customers ) );
    ships . push_back ( g_TestExtra[12] . PrepareTest ( "rth", customers ) );
    ships . push_back ( g_TestExtra[13] . PrepareTest ( "7Perth", customers ) );
    ships . push_back ( g_TestExtra[14] . PrepareTest ( "P56erth", customers ) );
    ships . push_back ( g_TestExtra[15] . PrepareTest ( "Per56th", customers ) );
    ships . push_back ( g_TestExtra[16] . PrepareTest ( "Perth56", customers ) );

    // add more ships here
  
  for ( auto x : customers )
    test . Customer ( x );
  
  test . Start ( 4, 22 );
  
  for ( auto x : ships )
    test . Ship ( x );

  test . Stop  ();

  for ( auto x : ships ) {
      bool valid = x -> Validate ();
      cout << x->Destination() << ": " << (valid ? "ok" : "fail") << endl;
      if (!valid)
          cout << "================================================================NOT VALID"<<endl;
  }
  return 0;  
}
#endif /* __PROGTEST__ */ 

